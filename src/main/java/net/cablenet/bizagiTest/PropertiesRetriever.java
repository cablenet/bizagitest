package net.cablenet.bizagiTest;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter @Setter @NoArgsConstructor
public class PropertiesRetriever {
    @Value("${cablenet.property.xml}")
    private String xml;
    @Value("${server.port}")
    private String serverPort;
    @Value("${cablenet.property.entityManagerSOA}")
    private String bizagientityManagerSOA;
}
