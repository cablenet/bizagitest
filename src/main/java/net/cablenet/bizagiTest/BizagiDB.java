package net.cablenet.bizagiTest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class BizagiDB implements CommandLineRunner {
    private final JdbcTemplate jbdcTemplate;

    private List<Map<String, Object>> results;

    @Autowired
    BizagiDB(JdbcTemplate jbdcTemplate) {
        this.jbdcTemplate = jbdcTemplate;
    }

    public void run(String... args) throws Exception {
    }

    public List getAllSubscriptions() {
        this.results = this.jbdcTemplate.queryForList("SELECT * FROM CABLENET_DEV.SUBSCRIPTION");
        return this.results;
    }

}
