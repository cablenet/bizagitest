package net.cablenet.bizagiTest;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@EnableAutoConfiguration
public class BizagiSOAP {

    private static final Logger log = LoggerFactory.getLogger(BizagiSOAP.class);

    @Autowired
    PropertiesRetriever pr;

    public String getEntityData(String entity, String field, String value) {
        String xmlPath = this.pr.getXml();
        try {
            String xml = new String(Files.readAllBytes(Paths.get(xmlPath + "/getBizagiEntities.xml")));
            xml = String.format(xml, entity, field + " = '" + value + "'");

            log.info("Bizagi getEntity >>> " + entity + " where " + field + " = '" + value + "'");

            HttpPost postRequest = new HttpPost(this.pr.getBizagientityManagerSOA());
            CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().build();

            StringEntity input = new StringEntity(xml);
            input.setContentType("text/xml");
            postRequest.setEntity(input);
            HttpResponse response = closeableHttpClient.execute(postRequest);
            String stringResponse = new BasicResponseHandler().handleResponse(response);
            log.info("<<<" + stringResponse);
            return stringResponse;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String sendSMS(String MSISDN, String requestType) {
        String xmlPath = this.pr.getXml();
        try {
            String xml = new String(Files.readAllBytes(Paths.get(xmlPath + "/sendSMS.xml")));
            xml = String.format(xml, MSISDN, requestType);

            log.info("Bizagi sendSMS >>> " + MSISDN + " : " + requestType );

            HttpPost postRequest = new HttpPost(this.pr.getBizagientityManagerSOA());
            CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().build();

            StringEntity input = new StringEntity(xml);
            input.setContentType("text/xml");
            postRequest.setEntity(input);
            HttpResponse response = closeableHttpClient.execute(postRequest);
            String stringResponse = new BasicResponseHandler().handleResponse(response);
            log.info("<<<" + stringResponse);
            return stringResponse;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
