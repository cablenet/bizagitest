package net.cablenet.bizagiTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BizagiTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BizagiTestApplication.class, args);
	}

}
