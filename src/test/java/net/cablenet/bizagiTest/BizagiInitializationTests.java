package net.cablenet.bizagiTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author e.lada
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@EnableAutoConfiguration
public class BizagiInitializationTests {

	@Autowired
	BizagiDB BizagiDatabase;

	@Autowired
	BizagiSOAP BizagiWebService;

	@Autowired
	public BizagiInitializationTests() {
		super();
	}

	@Test
	public void getAllSubscriptionsWorks(){
		List result = BizagiDatabase.getAllSubscriptions();

		Assert.assertNotNull(result);
	}

	@Test
	public void getEntityDataWorks(){
		String subscriptionNumber = "20200028";
		String result = BizagiWebService.getEntityData("SUBSCRIPTION", "SUBSCRIPTIONNUMBER", subscriptionNumber);

		Assert.assertNotNull(result);

	}


	@Test
	public void sendSMSWorks(){
		String telephone = "94094466";
		String requestType = "PAC";

		String result = BizagiWebService.sendSMS(telephone, requestType);


		Assert.assertNotNull(result);
	}

}